<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FrontController@index')->name('home');
Route::get('/shirts','FrontController@shirts')->name('shirts');
Route::get('/shirt','FrontController@shirt')->name('shirt');
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');  
Route::get('/home', 'HomeController@index');
Route::resource('/cart', 'CartController');
Route::get('/cart/add-items/{id}', 'CartController@addItem')->name('cart.addItem');

Route::group(['prefix'=>'admin','middleware'=>['auth','admin'] ],function(){
    Route::post('toggledeliver/{orderId}', 'OrderController@toggledeliver')->name('toggle.deliver');

    Route::get('/',function(){
        return view('admin.index');
    })->name('admin.index');


    Route::resource('product','ProductsController');
    Route::resource('category','CategoriesController');
    Route::get('orders/{type?}','OrderController@Orders');
    
    
});
Route::resource('address','AddressController');
//Route::get('checkout','Checkout@step1');
Route::group(['middleware'=>'auth'], function(){

    Route::get('shipping-info','Checkout@shipping')->name('checkout.shipping');


});

Route::get('upload','AddressController@showForm');
Route::post('upload','AddressController@storecsv');


Route::get('csv_file', 'CsvFile@index');
Route::get('export', 'CsvFile@excel');


Route::post('csv_file/import', 'CsvFile@csv_import')->name('import');

//Route::get('checkout','Checkout@step1');

Route::get('payment','Checkout@payment')->name('checkout.payment');
Route::post('store-payment','Checkout@storePayment')->name('payment.store');




