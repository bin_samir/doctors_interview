@extends('admin.layout.admin')

@section('content')


    <h3>Products</h3>

    <ul>
        @forelse( $products as $product)
            

            <li>
                
                <div class="form-group">
                <a href="{{action('ProductsController@edit',$product['id'])}}">EDIT</a>
                </div>
                
                <h4>Name of product:{{$product->name}}</h4>
                <h4>Category:{{$product->category->name}}</h4>

            <form action="{{route('product.destroy',$product->id)}}" method="POST">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <input class="btn btn-sm btn-danger" type="submit" value="Delete">
            </form>
            
            {!! Form::close() !!}
            <br>
            




            </li>

           

        @empty
        <h3>No products</h3>

        @endforelse
        <li>
            <form action="{{url('/upload')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="upload-file">upload csv</label>
                    <input type="file" name="upload-file" class="form-control">
                </div>
                <input class="btn btn-success" type="submit" value="Upload Image" name="submit">
                 </form>

                <form action="{{url('/csv_file')}}" method="GET" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="upload-file">export users to csv</label>
                        
                    </div>
                    <input class="btn btn-success" type="submit" value="export users table" name="submit">
            </form>

            </li>

    </ul>


    @endsection