@extends('admin.layout.admin')

@section('content')

<table class="table table-bordered table-striped">
 <thead>
  <tr>
   <th>Name</th>
   <th>Email Address</th>
  </tr>
 </thead>
 <tbody>
 @foreach($data as $row)
  <tr>
   <td>{{ $row->name }}</td>
   <td>{{ $row->email }}</td>
  </tr>
 @endforeach
 </tbody>
</table>

{!! $data->links() !!}
<form action="{{url('/export')}}" method="GET" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="form-group">
        <label for="upload-file">export users to csv</label>
        
    </div>
    <input class="btn btn-success" type="submit" value="export users table" name="submit">
</form>

@endsection