@extends('layout.main')

@section('content')

<div class="row">
    <h3>Cart Items</h3>

    <table class="table table-hover">
        <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>qty</th>
                <th>size</th>
                <td></td>
            </tr>
        </thead>
        <tbody>
        @foreach($cartItems as $cartItem)
            <tr>
                <td>{{$cartItem->name}}</td>
                <td>{{$cartItem->price}}</td>
                <td width="30px">
                    {{$cartItem->qty}}
                    {!! Form::open(['route'=>['cart.update', $cartItem->rowId],'method'=>'PUT']) !!}
                    <input name="qty" type="text" value="{{$cartItem->qty}}">
                    <input type="submit" class="btn btn-sm btn-default" value="Ok">
                    {!! Form::close() !!}
                </td>
                <td>{{$cartItem->options->has('size')?$cartItem->options->size:''}}</td>
                

                <td>


                <form action="{{route('cart.destroy',$cartItem->rowId)}}" method="POST">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <input class="button" type="submit" value="delete">
                    
                    





                </form>
                </td>

            </tr>
            @endforeach
            <tr>
                
                <td></td>
                <td>
                    subtotal : ${{Cart::subtotal()}}
                    
                    <br>
                    tax : ${{Cart::tax()}}
                    <br>
                    Grand Total : ${{Cart::total()}}
                </td>
                <td>Items : {{Cart::count()}}
                </td>
                <td></td>
                <td></td>

            </tr>
        </tbody>
    </table>
   


    <a href="{{route('checkout.shipping')}}" class="button">Check Out</a>
</div>
    @endsection