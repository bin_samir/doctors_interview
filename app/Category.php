<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{


    protected $fillable = [
        'name',
    ];
    public function products(){
        //telling that this category has many products
        //did it in the second way to help other coders reach class easily

        
        //return $this->hasMany('App/Product');
        return $this->hasMany(Product::class);
    }
}
