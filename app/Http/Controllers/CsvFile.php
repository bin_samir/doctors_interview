<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Excel;

class CsvFile extends Controller
{
    function index(){
        $data = User::latest()->paginate(10);
        return view ('admin.product.csv_file_pagination', compact('data'))->with('i',(request()->input('page',1)-1)*10);

    }
    function excel(){
        $customer_data = DB::table('users')->get()->toArray();
        $customer_array[] = array('name', 'email', 'password', 'remember_token', 'admin');
        foreach($customer_data as $customer)
        {
         $customer_array[] = array(
          'name'  => $customer->name,
          'email'   => $customer->email,
          'password'    => $customer->password,
          'remember_token'  => $customer->remember_token,
          'admin'   => $customer->admin
         );
        }
        Excel::create('Customer Data', function($excel) use ($customer_array){
         $excel->setTitle('Customer Data');
         $excel->sheet('Customer Data', function($sheet) use ($customer_array){
          $sheet->fromArray($customer_array, null, 'A1', false, false);
         });
        })->download('xlsx');
         
    }
}