<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class CsvExport extends Model
{
    public function collection()
    {
        return User::all();
    }
}
